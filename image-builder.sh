#!/bin/sh

sudo rm -rf tmp
cp -r etc tmp
mkdir -p cache out
cd tmp

ln -s ../cache cache

cp "../flavors/$1.list.chroot" config/package-lists/

if [ $1 = "desktop" ]; then
  sed -i 's/true/false/g' auto/config
fi

sudo lb build
sudo mv *.iso ../out/

cd ..
